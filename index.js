console.log(`READINGS-3 ACTIVITY`)

// Target each input field and assign variable
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const txtEmail = document.querySelector("#txt-email");
const pwPassword = document.querySelector("#pw-password");
const pwConfirm = document.querySelector("#pw-confirm");
const myDetails = document.querySelector("#my-details");
const myResponse = document.querySelector("#my-response");

// Create a function called getInfo
const getInfo = (e) => {
	if (txtFirstName.value.length === 0 ||
		txtLastName.value.length === 0 ||
		txtEmail.value.length === 0){
		console.log(`Please fill in your information`);
	} else if (pwPassword.value.length < 8){
		console.log(`Password length is less than 8 characters`);
	} else if (pwPassword.value !== pwConfirm.value){
		console.log(`Passwords do not match, please check again`);
	} else {
		console.log(`Thank you for logging in`);
	}
}

var concatDetails = (e) => {
	myDetails.innerHTML = [
		txtFirstName.value,
		txtLastName.value,
		txtEmail.value]
		.join(' ')
}

txtFirstName.addEventListener('keyup', concatDetails);
txtLastName.addEventListener('keyup', concatDetails);
txtEmail.addEventListener('keyup', concatDetails);

txtFirstName.addEventListener('change', getInfo);
txtLastName.addEventListener('change', getInfo);
txtEmail.addEventListener('change', getInfo);
pwPassword.addEventListener('change', getInfo);
pwConfirm.addEventListener('change', getInfo);